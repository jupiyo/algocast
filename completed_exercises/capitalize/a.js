<template>
    <div class="rsi-consult__operations">
        <v-form class="rsi-searcher-form">
            <h3>{{$t('consult.advanced_search')}}</h3>
            <v-layout class="rsi-searcher-form--primary">
                <v-flex xs4>
                    <v-select
                        :class="agreementListAux.length === 2 ? 'rsi-searcher-form__select input-group--select--no-options' : 'rsi-searcher-form__select' "
                        :label="agreementListAux.length === 2 ? $t('consult.agreement_selected') : $t('consult.agreement_select')"
                        v-model="agreementId"
                        :items="agreementListAux"
                        :error-messages="errors.collect('agreement_id')"
                        data-vv-name="agreement_id"
                        data-vv-as="select"
                        :readonly="agreementListAux.length === 2"
                        :append-icon="agreementListAux.length === 2 ? '' : keyboard_arrow_down"
                        @change="loadSelectUser"
                    >
                        <template slot="selection" scope="data">
                            <v-flex v-if=" data.item.DESCR_TXT != 'Todos'" class="ellipsis-text">
                                {{ data.item.CTA_ASO }} | {{ data.item.DESCR_TXT }}
                            </v-flex>
                            <v-flex v-else class="ellipsis-text">
                                {{ data.item.DESCR_TXT }}
                            </v-flex>
                        </template>
                        <template slot="item" scope="data">
                            <v-flex v-if=" data.item.DESCR_TXT != 'Todos'" class="ellipsis-text">
                                {{ data.item.CTA_ASO }} | {{ data.item.DESCR_TXT }}
                            </v-flex>
                            <v-flex v-else class="ellipsis-text">
                                {{ data.item.DESCR_TXT }}
                            </v-flex>
                        </template>
                    </v-select>
                </v-flex>
                <v-flex xs2>
                    <v-text-field
                        class="error-bubble datepicker-text"
                        :label="$t('consult.date_from')"
                        v-model="form.dateFromFormatted"
                        data-vv-name="fromDate"
                        data-vv-as="fecha desde"
                        :error-messages="errors.collect('fromDate')"
                        v-validate="{
                            rules: {
                                required: true,
                                validDate: form.dateFrom,
                                validDateOld: true,
                                validDateSearchFrom: form.dateToFormatted,
                            }
                        }"
                        slot="activator"
                        @blur="form.dateFromFormatted = getFormattedDate(form.dateFromFormatted).dmyBarr; form.dateFrom = getFormattedDate(form.dateFromFormatted).ymd;"
                        mask="##/##/####"
                    ></v-text-field>
                    <v-menu
                        transition="scale-transition"
                        class="helper-block"
                    >
                        <v-text-field
                            class="datepicker-icon"
                            slot="activator"
                            append-icon="today"
                            readonly
                        ></v-text-field>
                        <v-date-picker
                            :value="form.dateFrom"
                            locale="es-sp"
                            :first-day-of-week="1"
                            year-icon="create"
                            @input="form.dateFromFormatted = getFormattedDate($event).dmyBarr; form.dateFrom = getFormattedDate($event).ymd;"
                        ></v-date-picker>
                    </v-menu>
                </v-flex>
                <v-flex xs2>
                    <v-text-field
                        class="error-bubble datepicker-text"
                        :label="$t('consult.date_to')"
                        v-model="form.dateToFormatted"
                        slot="activator"
                        data-vv-name="toDate"
                        data-vv-as="fecha hasta"
                        :error-messages="errors.collect('toDate')"
                        v-validate="{
                            rules: {
                                required: true,
                                validDate: form.dateTo,
                                validDateOld: true,
                                validDateSearchTo: form.dateFromFormatted,
                            }
                        }"
                        @blur="form.dateToFormatted = getFormattedDate(form.dateToFormatted).dmyBarr; form.dateTo = getFormattedDate(form.dateToFormatted).ymd;"
                        mask="##/##/####"
                    ></v-text-field>
                    <v-menu
                        transition="scale-transition"
                        class="helper-block"
                    >
                        <v-text-field
                            class="datepicker-icon"
                            slot="activator"
                            append-icon="today"
                            readonly
                        ></v-text-field>
                        <v-date-picker
                            :value="form.dateTo"
                            locale="es-sp"
                            year-icon="create"
                            @input="form.dateToFormatted = getFormattedDate($event).dmyBarr; form.dateTo = getFormattedDate($event).ymd;"
                            :first-day-of-week="1"
                        ></v-date-picker>
                    </v-menu>
                </v-flex>
                <v-flex xs4>
                    <v-select
                        :label="$t('consult.search_by')"
                        v-model="searchBy"
                        :items="form.searchByItems"
                        item-text="name"
                        item-value="value"
                        :error-messages="errors.collect('search_id')"
                        data-vv-name="search_id"
                        data-vv-as="select"
                        append-icon="keyboard_arrow_down"
                        hide-details
                        clearable
                    ></v-select>
                </v-flex>
            </v-layout>
            <transition name="fade">
                <v-layout class="rsi-searcher-form--secondary">
                    <v-flex v-if="agreementId" xs4>
                        <v-select
                            :label="$t('consult.select_user')"
                            v-model="form.user"
                            :items="selectuser"
                            item-text="nombreUsuario"
                            item-value="usuario"
                            append-icon="keyboard_arrow_down"
                            autocomplete
                            bottom
                            :disabled="loadUserList"
                            clearable
                        ></v-select>
                    </v-flex>
                    <v-flex xs4>
                        <v-select
                            v-if="searchBy === 1"
                            :label="$t('consult.state_operation')"
                            v-model="form.situation"
                            :items="statusLoan"
                            item-text="VALOR"
                            item-value="CLAVE"
                            append-icon="keyboard_arrow_down"
                            persistent-hint
                            autocomplete
                            bottom
                        ></v-select>
                        <v-text-field
                            v-if="searchBy === 2"
                            class="rsi-searcher-form--secondary-text"
                            :label="$t('consult.loanNumber')"
                            v-model="form.loanNumber"
                            append-icon="search"
                            persistent-hint
                        ></v-text-field>
                        <v-text-field
                            v-if="searchBy === 3"
                            class="rsi-searcher-form--secondary-text"
                            :label="$t('consult.first_buyer_document')"
                            v-model="form.document"
                            append-icon="search"
                            persistent-hint
                        ></v-text-field>
                    </v-flex>
                    <v-flex xs0 class="rsi-searcher-form__action">
                        <v-btn
                            class="rsi-button--type-1 right" 
                            @click="validateSearch(false)"
                        >
                            {{ $t('consult.search') }}
                        </v-btn>
                    </v-flex>
                    <v-flex v-if="this.user.type === 'A'" xs2 class="rsi-searcher-form__action">
                        <v-btn
                            class="rsi-button--type-1 right" 
                            @click="validateSearch(true)"
                        >
                            {{ $t('consult.searchAll') }}
                        </v-btn>
                    </v-flex>
                </v-layout>
            </transition>
        </v-form>
        <div v-if="agreementView && !amortizationTable" class="rsi-searcher-form__result">
            <v-flex class="rsi-searcher-form__result__doc-actions">
                <v-btn
                    :ripple="false"
                    flat
                    @click="excel"
                >
                    <div class="rsi-wrap__icon-1">
                        <svgicon
                            icon="excel-icon"
                            :original="true"
                        ></svgicon>
                    </div>
                </v-btn>
                <v-btn
                    :ripple="false"
                    flat
                    @click="print"
                >
                    <div class="rsi-wrap__icon-2">
                        <svgicon
                            icon="print-icon"
                            :original="true"
                        ></svgicon>
                    </div>
                </v-btn>
            </v-flex>
            <h3>{{$t('consult.search_results')}}</h3>
            <v-data-table
                id="excel"
                :headers="table.headers"
                :items="operationsnoneconomic"
                :search="table.search"
                class="datatable--type-1 datatable--type-clickable"
                item-key="id"
                :hide-actions="operationsnoneconomic.length<6"
                :rows-per-page-items='[5, 10, { text: $t("consult.all"), value: -1 }]'
                :rows-per-page-text="$t('consult.rows_per_page')"
                :no-results-text="$t('consult.no_results_text')"
                :no-data-text="$t('consult.no_results_text')"
                >
                
                <template slot="items" scope="props">
                    <tr @click="operationDetails(props)" :class="parseFloat(props.item.situacion) == 23 ? 'state-active' : ''">
                        <td>{{ props.item.acuerdoConsumo }}</td>
                        <td v-if="parseFloat(props.item.acuerdoPrestamo) != 0">
                            {{ props.item.acuerdoPrestamo }}
                        </td>
                        <td v-else></td>
                        <td>{{ getFormattedDate(props.item.fechaAlta).dmy }}</td>
                        <td v-if="user.type === 'A'">{{ props.item.nombreComercio }}</td>
                        <td v-if="parseFloat(props.item.situacion) == 23" class="state text-xs-left">
                            <span>{{ situationOperation[parseFloat(props.item.situacion)] }}</span>
                            <v-tooltip v-if="parseFloat(props.item.situacion) == 23" right>
                                <v-btn slot="activator">
                                    <span class="wrap-icon">
                                        <svgicon
                                            icon="hand-icon"
                                            :original="true"
                                        ></svgicon>
                                    </span>
                                </v-btn>
                                <span class="tooltip--size-short">{{$t('consult.tooltip_pending')}}</span>
                            </v-tooltip>
                        </td>
                        <td v-else-if="parseFloat(props.item.situacion) == 6" class="state text-xs-left">
                           {{ situationOperation[parseFloat(props.item.situacion)] }}
                        </td>
                        <td class="state text-xs-left" v-else-if="parseFloat(props.item.estado) > 3" >
                               {{ stateOperation[parseFloat(props.item.estado)] }}
                            </td>
                        <td class="state text-xs-left" v-else> {{ situationOperation[parseFloat(props.item.situacion)]  }}</td>
                        <td>{{ props.item.nombrePrimerTitular }}</td>
                        <td>{{ props.item.idExternoPrimerTitular }}</td>
                        <td>{{ props.item.formaPago }}</td>
                        <td>{{ formatPrice(parseFloat(props.item.importe)).formatted }}</td>
                        <td v-if="props.item.duracion">{{ parseFloat(props.item.duracion) }}</td>
                        <td v-else></td>
                        <td v-if="props.item.descuento  && parseFloat(props.item.estado) == 4">
                            {{ formatPrice(props.item.descuento).formatted}}&nbsp;%
                        </td>
                        <td v-else></td>
                    </tr>
                </template>
                <template slot="expand" scope="props">
                    <v-layout v-if="parseFloat(props.item.situacion) != 6" grey lighten-3 py-2>
                        <v-flex xs3>
                            <p v-if="props.item.nombreComercio">
                                {{$t('consult.trade')}}
                            </p>
                            <p v-if="props.item.user">
                                {{$t('consult.user')}}
                            </p>
                            <p v-if="loanData.cuentaDomiciliacion && parseFloat(loanData.cuentaDomiciliacion) !== 0">
                                {{$t('consult.debit_account')}}
                            </p>
                            <p v-if="props.item.nombreSegundoTitular">
                                {{$t('consult.second_buyer')}}
                            </p>
                            <p v-if="props.item.idExternoSegundoTitular">
                                {{$t('consult.second_buyer_document')}}
                            </p>
                            <p v-if="loanData.cuotasCarencia">
                                {{$t('consult.grace_period_fee')}}
                            </p>
                            <p v-if="loanData.periodicidad">
                                {{$t('consult.periodicity')}}
                            </p>
                            <p v-if="loanData.estado">
                                {{$t('consult.loan_state')}}
                            </p>
                            <p v-if="loanData.fechaActivacion">
                                {{$t('consult.activation_date')}}
                            </p>
                            <p v-if="loanData.fechaPrimeraCuota">
                                {{$t('consult.first_fee_date')}}
                            </p>
                            <p v-if="loanData.fechaFin">
                                {{$t('consult.end_date')}}
                            </p>
                        </v-flex>
                        <v-flex xs4>
                            <p v-if="props.item.nombreComercio">
                                <strong>{{ props.item.nombreComercio}}</strong></p>
                            <p v-if="props.item.user">
                                <strong> {{ props.item.user }}</strong></p>
                            <p v-if="loanData.cuentaDomiciliacion && parseFloat(loanData.cuentaDomiciliacion) !== 0">
                                <strong>{{ formatIBAN(loanData.cuentaDomiciliacion) }}</strong></p>
                            <p v-if="props.item.nombreSegundoTitular">
                                <strong>{{ props.item.nombreSegundoTitular }}</strong></p>
                            <p v-if="props.item.idExternoSegundoTitular">
                                <strong>{{ props.item.idExternoSegundoTitular }}</strong></p>
                            <p v-if="loanData.cuotasCarencia">
                                <strong>{{ formatPrice(loanData.cuotasCarencia).num }}</strong></p>
                            <p v-if="loanData.periodicidad">
                                <strong>{{ periodicityCoding[loanData.periodicidad] }}</strong></p>
                            <p v-if="loanData.estado">
                                <strong>{{ state[parseFloat(loanData.estado)].text }}</strong></p>
                            <p v-if="loanData.fechaActivacion">
                                <strong>{{ getFormattedDate(loanData.fechaActivacion).dmy }}</strong></p>
                            <p v-if="loanData.fechaPrimeraCuota">
                                <strong>{{ getFormattedDate(loanData.fechaPrimeraCuota).dmy }}</strong></p>
                            <p v-if="loanData.fechaFin">
                                <strong>{{ getFormattedDate(loanData.fechaFin).dmy }}</strong></p>
                        </v-flex>
                        <v-flex xs3>
                            <p v-if="props.item.importeBien">
                                {{$t('consult.buy_price')}}
                            </p>
                            <p v-if="loanData.primeraCuota">
                                {{$t('consult.first_fee')}}
                            </p>
                            <p v-if="loanData.gastosApertura">
                                {{$t('consult.open_expense')}}
                            </p>
                            <p v-if="loanData.tae">
                                {{$t('consult.TAE')}}
                            </p>
                            <p v-else-if="props.item.tae">
                                {{$t('consult.TAE')}}
                            </p>
                            <p v-if="loanData.interesTotales">
                                {{$t('consult.total_interests')}}
                            </p>
                            <p v-if="loanData.importeTotal">
                                {{$t('consult.total_amount')}}
                            </p>
                            <p>
                                <v-btn
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="generateINE(props.item)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.generate_INE_and_TGSS')}}</strong>
                                </v-btn>
                            </p>
                            <p v-if="parseFloat(props.item.tipoOperacion) == 113 || parseFloat(props.item.tipoOperacion) == 133" >
                                <v-btn
                                    v-if="props.item.clipIdIne"
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="consultINE(props.item.clipIdIne)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.consult_INE_and_TGSS')}}</strong>
                                </v-btn>
                            </p>
                            <p v-else>
                                <v-btn
                                    v-if="loanData.clipIdIne"
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="consultINE(loanData.clipIdIne)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.consult_INE_and_TGSS')}}</strong>
                                </v-btn>
                            </p>
                            <p>
                                <v-btn
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="generatePolicy(props.item)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.generate_policy')}}</strong>
                                </v-btn>
                            </p>
                            <p v-if="parseFloat(props.item.tipoOperacion) == 113 || parseFloat(props.item.tipoOperacion) == 133" >
                                <v-btn
                                    v-if="props.item.clipIdPoliza"
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="consultPolicy(props.item.clipIdPoliza)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.consult_policy')}}</strong>
                                </v-btn>
                            </p>
                            <p v-else >
                                <v-btn
                                    v-if="loanData.clipIdPoliza"
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link--with-icon"
                                    @click="consultPolicy(loanData.clipIdPoliza)"
                                >
                                    <span class="rsi-wrap-icon"><v-icon class="rsi-icon-pdf">fa fa-file-pdf-o</v-icon></span>
                                    <strong>{{$t('consult.consult_policy')}}</strong>
                                </v-btn>
                            </p>
                            <p>
                                <v-btn
                                    :ripple="false"
                                    flat
                                    class="rsi-pdf-link"
                                    @click="openAmortizationTable(props.item.acuerdoPrestamo,props.item.estado,props.item.situacion, props.item.tae, parseFloat(props.item.tipoOperacion))"
                                >
                                    <p class="rsi-pdf-link">{{$t('consult.amortization_table')}}</p>
                                </v-btn>
                            </p>
                        </v-flex>
                        <v-flex xs3>
                            <p v-if="props.item.importeBien">
                                <strong>{{ formatPrice(props.item.importeBien).formatted }} €</strong></p>
                            <p v-if="loanData.primeraCuota">
                                <strong>{{ formatPrice(loanData.primeraCuota).formatted }} € </strong></p>
                            <p v-if="loanData.gastosApertura">
                                <strong>{{ formatPrice(loanData.gastosApertura).formatted }} %</strong></p>
                            <p v-if="loanData.tae && parseFloat(props.item.tipoOperacion) != 113 && parseFloat(props.item.tipoOperacion) != 133">
                                <strong>{{ formatPriceTAE(parseFloat(loanData.tae)).formatted }} %</strong></p>
                            <p v-else-if="props.item.tae">
                                <strong>{{ formatPriceTAE(parseFloat(props.item.tae)).formatted }} %</strong></p>
                            <p v-if="loanData.interesTotales">
                                <strong>{{ formatPrice(loanData.interesTotales).formatted }} €</strong></p>
                            <p v-if="loanData.importeTotal">
                                <strong>{{ formatPrice(loanData.importeTotal).formatted }} €</strong></p>
                        </v-flex>
                    </v-layout>
                    <v-layout v-else grey lighten-3 row py-2>
                        <v-flex xs3>
                            <v-card-text v-if="props.item.descError" class="pb-0 pt-1 mb-0">
                                {{$t('consult.reason')}}
                            </v-card-text>
                        </v-flex>
                        <v-flex xs9>
                            <v-card-text v-if="props.item.descError" class="pb-0 pt-1 mb-0">
                                <strong><span v-if="user.type === 'A'"> {{ parseFloat(props.item.codError) }} - </span>{{ props.item.descError}}</strong></v-card-text>
                        </v-flex>
                    </v-layout>
                    <v-layout grey lighten-3 row py-2>
                        <v-flex v-if="user.type === 'A' && parseFloat(props.item.estado) != 4 && parseFloat(props.item.tipoOperacion) != 113 && parseFloat(props.item.tipoOperacion) != 133" xs0 class="rsi-searcher-form__action">
                            <v-btn 
                                class="rsi-button--type-1 right rsi-button--tramitar" 
                                @click="tramitarOperation(props.item)"
                            >
                                {{ $t('consult.process') }}
                            </v-btn>
                        </v-flex>
                    </v-layout>
                </template>
                <template slot="pageText" scope="item"> 
                        {{item.pageStart}} - {{item.pageStop}} {{$t('consult.of')}} {{item.itemsLength}}
                </template>
                
            </v-data-table>
            <div class="rsi-searcher-form__pagination">
                <v-flex>
                <rsi-button :class="paginaSelec == '1' ? 'rsi-button--type-5' : 'rsi-button--type-6'" button-text="<"
                    @click="getPage(paginaSelec-1)">
                </rsi-button>
                <span>{{(paginaSelec-1)*5+1}} - {{paginaSelec*5>=parseFloat(operationsnoneconomic.numRegistros) ? parseFloat(operationsnoneconomic.numRegistros) :(paginaSelec)*5}} de {{parseFloat(operationsnoneconomic.numRegistros)}}
                </span>
                <rsi-button :class="paginaSelec*5>=parseFloat(operationsnoneconomic.numRegistros) ? 'rsi-button--type-5' : 'rsi-button--type-6'" button-text=">"
                    @click="getPage(paginaSelec+1)">
                </rsi-button>
                </v-flex>
            </div>
        </div>
        <div v-else>
            <div v-if="amortizationTable" class="rsi-searcher-form__result__amortization">
                <rsi-amortization-table
                    :estado="estado"
                    :TAE="tae === NAN ? -1 : tae "
                >
                </rsi-amortization-table>
                <div class="rsi-wrap-button text-xs-center">
                    <rsi-button class="rsi-button--type-7" button-text="Volver"
                        @click="amortizationTable=false">
                    </rsi-button>
                </div>
            </div>
            <div v-if="noResult" class="rsi-searcher-form__result__amortization">
                <rsi-no-result
                    :title="$t('modify.ops')"
                    :subtitle="$t('modify.error_no_result')"
                ></rsi-no-result>
            </div>
            
        </div>
        
    </div>
</template>

<script>
  import {mapState} from 'vuex';
  import '@/assets/compiled-icons/excel-icon';
  import '@/assets/compiled-icons/print-icon';
  import '@/assets/compiled-icons/hand-icon';
  

  export default {
      
      
    data () {
      const today = new Date();
      const sevenDays = this.calculateDate(today, -7);
      let date7 = new Date();
      let date1 = sevenDays.getDate() + '/' + (sevenDays.getMonth() + 1) + '/' + sevenDays.getFullYear();
      let date2 = today.getDate() + '/' + (today.getMonth() + 1) + '/' + today.getFullYear();
      
      return {
        searchBy: null,
        agreementId: '',
        agreementView: false,
        agreementListAux: '',
        amortizationTable: false,
        acuerdoPrestamo: '',
        noResult: false,
        estado: '',
        situacion: '',
        auxProps: '',
        datos: [],
        paginas: [],
        numInicio: 1,
        numFin: 5,
        tae: '',
        paginaSelec: 1,
        operation: {},
        loadUserList: true,
        stateOperation: {
          7: this.$i18n.t('consult.canceled'),
          4: this.$i18n.t('consult.activated'),
        },
        situationOperation: {
          2: this.$i18n.t('consult.authorized'),
          6: this.$i18n.t('consult.reject'),
          23: this.$i18n.t('consult.pending'),
          33: this.$i18n.t('consult.in_study'),
        },
        state: [
          {
            id: 0,
            text: this.$i18n.t('consult.state_offered')
          },
          {
            id: 1,
            text: this.$i18n.t('consult.state_requested')
          },
          {
            id: 2,
            text: this.$i18n.t('consult.state_proposed')
          },
          {
            id: 3,
            text: this.$i18n.t('consult.state_approved')
          },
          {
            id: 4,
            text: this.$i18n.t('consult.state_active')
          },
          {
            id: 5,
            text: this.$i18n.t('consult.state_discontinued')
          },
          {
            id: 6,
            text: this.$i18n.t('consult.state_defeated')
          },
          {
            id: 7,
            text: this.$i18n.t('consult.state_cancelled')
          },
        ],
        periodicityCoding: {
          M: this.$i18n.t('consult.periodicity_monthly'),
          B: this.$i18n.t('consult.periodicity_bimonthly'),
          T: this.$i18n.t('consult.periodicity_quarterly'),
          S: this.$i18n.t('consult.periodicity_biannual'),
          A: this.$i18n.t('consult.periodicity_annual'),
          N: this.$i18n.t('consult.periodicity_without_assessing')
        },
        form: {
          searchByItems: [
            {
              name: this.$i18n.t('consult.search_by_items_operation'),
              value: 1
            },
            {
              name: this.$i18n.t('consult.search_by_items_loan_number'),
              value: 2
            },
            {
              name: this.$i18n.t('consult.search_by_items_doc_first_buyer'),
              value: 3
            },
          ],
          situation: [
            {
              name: this.$i18n.t('consult.situation_items_accepted'),
              value: 1
            },
            {
              name: this.$i18n.t('consult.situation_items_in_study'),
              value: 2
            },
            {
              name: this.$i18n.t('consult.situation_items_authorized'),
              value: 3
            },
            {
              name: this.$i18n.t('consult.situation_items_activated'),
              value: 4
            },
          ],
          user: null,
          dateFrom: this.getFormattedDate(this.calculateDate(today, -7)).ymd,
          dateFromFormatted: this.pad(sevenDays.getDate(),2) + '/' + this.pad((sevenDays.getMonth() + 1),2) + '/' + sevenDays.getFullYear(),
          dateTo: this.getFormattedDate(today).ymd,
          dateToFormatted: this.pad(today.getDate(),2) + '/' + this.pad((today.getMonth() + 1),2) + '/' + today.getFullYear(),
          document: '',
          loanNumber: '',
        },
        table: {
          search: '',
          headers: [
            {
              text: this.$i18n.t('consult.agreement'),
              align: 'left',
              sortable: false,
              value: 'acuerdoConsumo'
            },
            {text: this.$i18n.t('consult.loanNumber'), value: 'acuerdoPrestamo', sortable: false},
            {text: this.$i18n.t('consult.date_operation'), sortable: false, value: 'dateRegistration'},
            {text: this.$i18n.t('consult.state'), sortable: false, value: 'estado', align: 'left'},
            {text: this.$i18n.t('consult.first_buyer'), sortable: false, value: 'primerTitular'},
            {text: this.$i18n.t('consult.doc_first_buyer'), sortable: false, value: 'segundoTitular'},
            {text: this.$i18n.t('consult.pay_form'), sortable: false, value: 'formaPago'},
            {text: this.$i18n.t('consult.amountfinance'), sortable: false, value: 'amountFinance'},
            {text: this.$i18n.t('consult.no_grace_period'), sortable: false, value: 'carencia'},
            {text: this.$i18n.t('consult.discount'), sortable: false, value: 'descuento'}
          ]
        },
        dataSolicitud :{}
      }
    },
    computed: mapState([
      'agreementList',
      'amortization',
      'loanData',
      'statusLoan',
      'consumptionoperation',
      'centerarecuperacion',
      'centeradocument',
      'selectuser',
      'operationsnoneconomic',
      'contractNumber',
      'searchParameters',
      'user',
      'loan',
      'API_URL',
      'errorPopUp',
      'uipAll'
    ]),
    created() {
        this.agreementListAux = [];
        for ( var i = 0; i < this.agreementList.length; i++) {
            this.agreementListAux[ i ] = this.agreementList[ i ];
        }
        let aux = {
            CTA_ASO: "",
            DESCR_TXT: "Todos"
        }
        this.agreementListAux.push(aux);
        if(this.user.type === 'A'){
            this.table.headers = [
                {
                    text: this.$i18n.t('consult.agreement'),
                    align: 'left',
                    sortable: false,
                    value: 'acuerdoConsumo'
                },
                {text: this.$i18n.t('consult.loanNumber'), value: 'acuerdoPrestamo', sortable: false},
                {text: this.$i18n.t('consult.date_operation'), sortable: false, value: 'dateRegistration'},
                {text: this.$i18n.t('consult.trade_name'), sortable: false, value: 'tradeName'},
                {text: this.$i18n.t('consult.state'), sortable: false, value: 'estado'},
                {text: this.$i18n.t('consult.first_buyer'), sortable: false, value: 'primerTitular'},
                {text: this.$i18n.t('consult.doc_first_buyer'), sortable: false, value: 'segundoTitular'},
                {text: this.$i18n.t('consult.pay_form'), sortable: false, value: 'formaPago'},
                {text: this.$i18n.t('consult.amountfinance'), sortable: false, value: 'amountFinance'},
                {text: this.$i18n.t('consult.no_grace_period'), sortable: false, value: 'carencia'},
                {text: this.$i18n.t('consult.discount'), sortable: false, value: 'descuento'}
            ];
        }
        //llamada servicio que devuelve
        this.loadStore('LOAD_STATUSLOAN_LIST');
    },
    mounted () {
      if (this.agreementListAux.length === 2) {     /* Para este caso pregunta por 2 porque hay un elemento que se llama "TODOS" */
        this.agreementId = this.agreementListAux[0];
      }
    },
    methods: {
      handleError(action, response, data) {
          if (response == 404) {
              let error; 
            switch (action) {
                case 'LOAD_OPERATIONNOECONOMIC_LIST':
                    this.$root.$loading.finish();
                    this.noResult = true;
                    this.agreementView = false;
                    break;
                case 'LOAD_CENTERADOCUMENT_LIST':
                    this.$root.$loading.finish();
                    error = {
                        value : true,
                        text :  this.$i18n.t('error-pop-up.pdf'),
                    };
                    this.$store.commit('SET_ERROR_POP_UP', error);
                    break;
                case 'LOAD_CENTERARECUPERACION_LIST':
                    this.$root.$loading.finish();
                    error = {
                        value : true,
                        text :  this.$i18n.t('error-pop-up.pdf'),
                    };
                    this.$store.commit('SET_ERROR_POP_UP', error);
                    break;
                case 'LOAD_LOAN': 
                    this.$root.$loading.finish();
                    error = {
                        value : true,
                        text :  this.$i18n.t('error-pop-up.pdf'),
                    };
                    this.$store.commit('SET_ERROR_POP_UP', error);
                    break;
                default : 
                    this.getError(response);
                    break;
            }
          } else {
            this.getError(response);
          }
      },
      onSuccess(action, response, data) {
        switch (action) {
          case 'LOAD_OPERATIONNOECONOMIC_LIST':
            this.step0();
            break;
          case 'LOAD_AMORTIZATION_LIST':
            this.step1();
            break;
            // TODO: OJO!!! se llama dos veces con distintos métodos.
          case 'LOAD_LOANDATA_LIST':
            this.step3();
            break;
          case 'LOAD_CENTERADOCUMENT_LIST':
            this.step4();
            break;
        case 'LOAD_OPERATIONNOECONOMIC_DETAILS':
            this.step5();
            break;
        case 'LOAD_CENTERARECUPERACION_LIST':
            this.step6();
            break;
        case 'LOAD_LOAN': 
            if (this.dataSolicitud.cuentaConsumo) {
                this.step2Tramitar();
            } else {
                this.step1Policy();
            }
            break;
        case 'LOAD_SELECTUSER_LIST':
            this.loadUserList = false;
            break;
        case 'LOAD_STATUSLOAN_LIST':
            this.loadStore('LOAD_SELECTUSER_LIST', this.contractNumber);
            break;
        case 'LOAD_UIP_ALL': 
            this.step1Tramitar();
            this.$root.$loading.finish();
            break;
        }
      },
      tramitarOperation(operation) {
        this.$root.$loading.start();        
        this.operation = operation;
        let data = {
            clave: this.operation.numOrdenPpal,
        }
        this.loadStore('LOAD_UIP_ALL',  data);
      },
      step1Tramitar() {
        let importeBien = this.operation.importeBienMod ? this.operation.importeBienMod : this.operation.importeBien;
        let importe = this.operation.importeMod ? this.operation.importeMod : this.operation.importe;
        let duracion = this.operation.plazoMod ? this.operation.plazoMod : this.operation.duracion;
        this.dataSolicitud = {
            cuentaConsumo : this.operation.cuentaConsumo,
            formaPago: this.operation.formaPago,
            idExternoPrimerTitular: this.operation.idExternoPrimerTitular,
            idExternoSegundoTitular: this.operation.idExternoSegundoTitular,
            importe: importe,
            duracion: duracion,
            finalidad: this.operation.finalidad,
        }
        if (this.operation.situacion == '6') {
            this.loadStore('LOAD_LOAN', this.operation.numOrdenPpal);
        } else {
            this.dataSolicitud['fechaCaducidadDocumentoPrimerTitular'] = this.operation.fechaCaducidadDocumentoPrimerTitular;
            this.dataSolicitud['fechaCaducidadDocumentoSegundoTitular'] = this.operation.fechaCaducidadDocumentoSegundoTitular;
            this.dataSolicitud['codigoIdExternoPrimerTitular'] = this.operation.codigoIdExternoPrimerTitular;
            this.dataSolicitud['codigoIdExternoSegundoTitular'] = this.operation.codigoIdExternoSegundoTitular;
            this.dataSolicitud['importeBien'] = importeBien;
            this.dataSolicitud['carencia'] = this.operation.carencia;
            this.dataSolicitud['cuentaDomiciliacion'] = this.operation.cuentaDomiciliacion;
            this.$store.commit('SET_PROCESS_NEW_OPERATION', this.dataSolicitud);
            this.$router.replace({path: '/solicitud'});
            this.$root.$loading.finish();
        }
     },
     step2Tramitar() {
        if (this.uipAll.Respuesta[0]) {
            this.dataSolicitud['fechaCaducidadDocumentoPrimerTitular'] = this.uipAll.Respuesta[0].fechaCaducidad;
            this.dataSolicitud['codigoIdExternoPrimerTitular'] = this.uipAll.Respuesta[0].codigoIdExterno;
        }
        if (this.uipAll.Respuesta[1]) {
            this.dataSolicitud['fechaCaducidadDocumentoSegundoTitular'] = this.uipAll.Respuesta[1].fechaCaducidad;
            this.dataSolicitud['codigoIdExternoSegundoTitular'] = this.uipAll.Respuesta[1].codigoIdExterno;
        }
        this.dataSolicitud['importeBien'] = this.loan.precioBien;
        this.dataSolicitud['carencia'] = this.loan.carencia;
        this.dataSolicitud['cuentaDomiciliacion'] = this.loan.cuentaDomiciliacion.substring(4,24);
        this.dataSolicitud['finalidad'] = this.loan.finalidad;
        this.$store.commit('SET_PROCESS_NEW_OPERATION', this.dataSolicitud);
        this.$router.replace({path: '/solicitud'});
        this.$root.$loading.finish();
     },
      getPage(numPages){
          this.paginaSelec = numPages;
          this.numInicio = (numPages-1)*5+1;
          this.numFin = this.numInicio+4;
          this.validateSearch();
      },
      step0() {
        this.paginas = this.getPagination(parseFloat(this.operationsnoneconomic.numRegistros), 5);
        if (!this.operationsnoneconomic[0]) {
          this.$root.$loading.finish();
          this.noResult = true;
          this.agreementView = false;
        } else {
          this.noResult = false;
          this.$root.$loading.finish();
          this.agreementView = true;
          this.datos = [];
          let operations = this.operationsnoneconomic;
          let length = operations.length;

            for(var i = 0; i < length; i++) {
                let operation = operations[i];
                let fila = [];
                fila.push(operation.acuerdoConsumo);
                if (parseFloat(operation.acuerdoPrestamo, 10) != 0) {
                    fila.push(operation.acuerdoPrestamo);
                } else {
                    fila.push('');
                };
                fila.push(this.getFormattedDate(operation.fechaAlta).dmy);
                if (parseFloat(operation.situacion, 10) == 6) {
                    fila.push(this.situationOperation[parseFloat(operation.situacion)]);
                } else if (parseFloat(operation.estado, 10) > 3) {
                    fila.push(this.stateOperation[parseFloat(operation.estado, 10)]);
                } else {
                    fila.push(this.situationOperation[parseFloat(operation.situacion, 10)]);
                }
                fila.push(operation.nombrePrimerTitular);
                fila.push(operation.idExternoPrimerTitular);
                fila.push(operation.formaPago);
                fila.push(this.formatPrice(operation.importe).formatted);
                if (operation.duracion) {
                    fila.push(parseFloat(operation.duracion));
                } else {
                    fila.push('');
                }
                if (operation.descuento) {
                    fila.push(this.formatPrice(operation.descuento).formatted);
                } else {
                    fila.push('');
                }
                this.datos.push(fila);
            }
        }
      },
      step1() {
        const total = {
            impCuotaIntegra: this.amortization.totalCuotaIntegra,
            impAmortizacion: this.amortization.totalAmortizacion,
            impSubvencionado: this.amortization.totalSubvencionado,
            impPendienteVenci: 0,
            impIntereses: this.amortization.totalIntereses,
            impIntSubvencionado: this.amortization.totalIntSubvencionado,
            impDemora: this.amortization.totalDemora,
            impComisiones: this.amortization.totalComisiones,
            impGastos: this.amortization.totalGastos,
            impPagado: this.amortization.totalPagado
        };
        this.amortization.ListaCuadro.push(total);
        this.$root.$loading.finish();
        this.amortizationTable = true
      },
      step3(){
        this.$root.$loading.finish();
        this.auxProps.expanded = !this.auxProps.expanded;
      },
      step4() {
        this.$root.$loading.finish();
        const hrefEl = document.getElementById('pdf');
        hrefEl.target = '_blank';
        hrefEl.href = 'pdf/' + this.centeradocument.response.data.nombreDocumento + ".pdf";
        hrefEl.click();
      },
      step5(){
        this.loadStore('LOAD_LOANDATA_LIST',  this.acuerdoPrestamo);
      },
      step6(){
        this.$root.$loading.finish();
        const hrefEl = document.getElementById('pdf');
        hrefEl.target = '_blank';
        hrefEl.href = 'pdf/' + this.centerarecuperacion.nombreDocumento + ".pdf";
        hrefEl.click();
      },
      validateSearch(admin) {
        this.$validator.validateAll().then(result => {
            //getFormattedDate(form.dateToFormatted).dmyBarr
          if (result) {
            this.$root.$loading.start();
            this.amortizationTable = false;
            let data = {};
            if (this.searchBy === 1) {
              data = {
                fechaDesde: this.form.dateFrom,
                fechaHasta: this.form.dateTo,
                usuario: this.form.user,
                estado: this.form.situation,
              }
            } else if (this.searchBy === 2) {
              data = {
                fechaDesde: this.form.dateFrom,
                fechaHasta: this.form.dateTo,
                usuario: this.form.user,
                acuerdoPrestamo: this.form.loanNumber
              }
            } else if (this.searchBy === 3) {
              data = {
                fechaDesde: this.form.dateFrom,
                fechaHasta: this.form.dateTo,
                usuario: this.form.user,
                idExterno: this.form.document,
              }
            } else {
              data = {
                fechaDesde: this.form.dateFrom,
                fechaHasta: this.form.dateTo,
              }
            }
            if (this.agreementId && this.agreementId.CTA_ASO != "") {
              data['cuentaConsumo'] = this.agreementId.CTA_ASO;
            }
            if (this.form.user) {
              data['usuario'] = this.form.user;
            }
            if (admin != true) {
                data['contrato'] = this.contractNumber;
            }
            data['inicioPaginacion'] = this.numInicio;
            data['finalPaginacion'] = this.numFin;
            data['codigoEntidad'] = this.user.codigoEntidad;

            this.loadStore('LOAD_OPERATIONNOECONOMIC_LIST', data);
          }
        });
      },
      openAmortizationTable(acuerdoPrestamo, estado, situacion, tae, operacion) {
        this.$root.$loading.start();
        this.acuerdoPrestamo = acuerdoPrestamo;
        this.estado = estado;
        this.situacion = situacion;
        if (operacion == 113 || operacion == 133) {
            this.tae = tae;
        } else {
            if (this.loanData.tae) {
                this.tae = this.loanData.tae
            } else {
                this.tae = tae;
            }
        }
        this.loadStore('LOAD_AMORTIZATION_LIST', acuerdoPrestamo);
      },
      excel() {
        var headers =
            [
            this.$i18n.t('consult.agreement'),
            this.$i18n.t('consult.loanNumber'),
            this.$i18n.t('consult.date_operation'),
            this.$i18n.t('consult.state'),
            this.$i18n.t('consult.first_buyer'),
            this.$i18n.t('consult.doc_first_buyer'),
            this.$i18n.t('consult.pay_form'),
            this.$i18n.t('consult.amountfinance'),
            this.$i18n.t('consult.no_grace_period'),
            this.$i18n.t('consult.discount')
            ];
        let datos = [];
        datos.push(headers);

        for(var i = 0; i < this.operationsnoneconomic.length; i++) {
            let fila = [];
            fila.push(this.operationsnoneconomic[i].acuerdoConsumo);
            if (parseFloat(this.operationsnoneconomic[i].acuerdoPrestamo) != 0) {
                fila.push(this.operationsnoneconomic[i].acuerdoPrestamo);
            } else {
                fila.push('');
            };
            fila.push(this.getFormattedDate(this.operationsnoneconomic[i].fechaAlta).dmy);
            if (parseFloat(this.operationsnoneconomic[i].situacion) == 6) {
                fila.push(this.situationOperation[parseFloat(this.operationsnoneconomic[i].situacion)]);
            } else if (parseFloat(this.operationsnoneconomic[i].estado) > 3) {
                fila.push(this.stateOperation[parseFloat(this.operationsnoneconomic[i].estado)]);
            } else {
                fila.push(this.situationOperation[parseFloat(this.operationsnoneconomic[i].situacion)]);
            }
            fila.push(this.operationsnoneconomic[i].nombrePrimerTitular);
            fila.push(this.operationsnoneconomic[i].idExternoPrimerTitular);
            fila.push(this.operationsnoneconomic[i].formaPago);
            fila.push(this.formatPrice(this.operationsnoneconomic[i].importe).formatted);
            if (this.operationsnoneconomic[i].duracion) {
                fila.push(parseFloat(this.operationsnoneconomic[i].duracion));
            } else {
                fila.push('');
            }
            if (this.operationsnoneconomic[i].descuento) {
                fila.push(this.formatPrice(this.operationsnoneconomic[i].descuento).formatted);
            } else {
                fila.push('');
            }
            datos.push(fila);
        } 
        
        const ws = XLSX.utils.aoa_to_sheet(datos);
        const wb = XLSX.utils.book_new();
        XLSX.utils.book_append_sheet(wb, ws, "Operaciones");
        // generate X file 
        const wbout = XLSX.write(wb, {type:"array", bookType:"xlsx"});
        // send to client 
        saveAs(new Blob([wbout],{type:"application/octet-stream"}), "Operaciones.xlsx"); 
       
      },

      print() {
        window.print();
        /* var ficha= document.getElementById("excel");
        console.log(document.getElementById("excel"));
        var ventimp= window.open(' ','popimpr');
        ventimp.document.write(ficha.innerHTML);
        ventimp.document.close();
        ventimp.print();
        ventimp.close(); */
      },
      loadSelectUser(selected) {
        this.loadStore('LOAD_SELECTUSER_LIST', this.contractNumber);
      },
      generatePolicy(operationsnoneconomic) {
        this.$root.$loading.start();
        this.loadStore('LOAD_LOAN', operationsnoneconomic.numOrdenPpal);
        this.operation = operationsnoneconomic; 
      },
      step1Policy() {
        const today = new Date();
        let importeTexto = '';
        let importe = '';
        let tae = '0';
        let comisionApertura = '';
        let comisionAmortizacion = '';
        let comisionCancelacion = '';
        let comisionEstudio = '';
        let importeTotalFinanciado = '';
        let entidad = this.agreementList[0].CTA_ASO.substring(0,4);
        let firma1 = entidad;
        let firma2 = entidad;
        if (parseFloat(this.operation.tipoOperacion) == 113 || parseFloat(this.operation.tipoOperacion) == 133 ) {
            tae = this.operation.tae;
        } else {
            if (this.loanData.tae) {
                tae = this.loanData.tae
            } else if (this.operation.tae) {
                tae = this.operation.tae;
            }
        }
        
        let fechaVencimiento = '';
        if (this.loanData.fechaFinPrestamo){
            fechaVencimiento = this.getFormattedDate(this.loanData.fechaFinPrestamo).dmy;
        }
        if (this.operation.importeMod) {
            importeTexto = this.operation.importeMod;
            importe = this.formatPrice(parseFloat(this.operation.importeMod)).formatted;
            comisionApertura = this.formatPrice(parseFloat(this.operation.importeMod)*parseFloat(this.loanData.comisionApertura)/100).formatted;
            comisionAmortizacion = this.formatPrice(parseFloat(this.operation.importeMod)*parseFloat(this.loanData.comisionAmortizacion)/100).formatted;
            comisionCancelacion = this.formatPrice(parseFloat(this.operation.importeMod)*parseFloat(this.loanData.comisionCanceAnti)/100).formatted;
            comisionEstudio = this.formatPrice(parseFloat(this.operation.importeMod)*parseFloat(this.loanData.comisionEstudio)/100).formatted;
            importeTotalFinanciado = importe;
            if (this.loan[0]) {
                console.log("No está vacio");
                if (this.loan[0].indComApertura == '1'){
                    console.log("Cambia el importe total financiado");
                    importeTotalFinanciado = this.formatPrice(parseFloat(this.operation.importeMod)+(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionApertura)/100)).formatted;
                } else {
                    console.log("No cambia el importe total financiado");
                }
            } else {
                console.log("Está vacio");
            }
        } else {
            importeTexto = this.operation.importe;
            importe =this.formatPrice(parseFloat(this.operation.importe)).formatted;
            comisionApertura = this.formatPrice(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionApertura)/100).formatted;
            comisionAmortizacion = this.formatPrice(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionAmortizacion)/100).formatted;
            comisionCancelacion = this.formatPrice(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionCanceAnti)/100).formatted;
            comisionEstudio = this.formatPrice(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionEstudio)/100).formatted;
            importeTotalFinanciado = importe;
            if (this.loan[0]) {
                console.log("No está vacio");
                if (this.loan[0].indComApertura == '1'){
                    console.log("Cambia el importe total financiado");
                    importeTotalFinanciado = this.formatPrice(parseFloat(this.operation.importe)+(parseFloat(this.operation.importe)*parseFloat(this.loanData.comisionApertura)/100)).formatted;
                } else {
                    console.log("No cambia el importe total financiado");
                }
            } else {
                console.log("Está vacio");
            }
        }
        let domicilioTipoViaSegundoComprador = ''; 
        let domicilioNombreViaSegundoComprador = ''; 
        let domicilioNumeroSegundoComprador = ''; 
        let domicilioProvinciaSegundoComprador = ''; 
        let domicilioLocalidadSegundoComprador = ''; 
        let domicilioPostalSegundoComprador = ''; 
        let domicilioPaisSegundoComprador = '';
        let domicilioPlantaSegundoComprador = '';
        let domicilioPuertaSegundoComprador = '';
        let existeSegundoComprador = 'noVisible';
        if (this.operation.domicilioTipoViaSegundoTitular){
            domicilioTipoViaSegundoComprador = this.operation.domicilioTipoViaSegundoTitular; 
            domicilioNombreViaSegundoComprador = this.operation.domicilioNombreViaSegundoTitular; 
            domicilioNumeroSegundoComprador = this.operation.domicilioNumeroSegundoTitular; 
            domicilioProvinciaSegundoComprador = this.operation.domicilioProvinciaSegundoTitular; 
            domicilioLocalidadSegundoComprador = this.operation.domicilioLocalidadSegundoTitular; 
            domicilioPostalSegundoComprador = this.operation.domicilioPostalSegundoTitular; 
            domicilioPaisSegundoComprador = this.operation.paisResidenciaSegundTitular;
            domicilioPlantaSegundoComprador = this.operation.domicilioPlantaSegundoTitular;
            domicilioPuertaSegundoComprador = this.operation.domicilioPuertaSegundoTitular;
            existeSegundoComprador = '';
        }
        if (this.operation.domicilioTipoViaAvalista){
            domicilioTipoViaSegundoComprador = this.operation.domicilioTipoViaAvalista; 
            domicilioNombreViaSegundoComprador = this.operation.domicilioNombreViaAvalista; 
            domicilioNumeroSegundoComprador = this.operation.domicilioNumeroAvalista; 
            domicilioProvinciaSegundoComprador = this.operation.domicilioProvinciaAvalista; 
            domicilioLocalidadSegundoComprador = this.operation.domicilioLocalidadAvalista; 
            domicilioPostalSegundoComprador = this.operation.domicilioPostalAvalista; 
            domicilioPaisSegundoComprador = this.operation.paisResidenciaAvalista;
            domicilioPlantaSegundoComprador = this.operation.domicilioPlantaAvalista;
            domicilioPuertaSegundoComprador = this.operation.domicilioPuertaAvalista;
            existeSegundoComprador = '';
        }
        let data = {
            codigoEntidad: this.user.codigoEntidad,
            cuenta: this.operation.cuentaConsumo,
            acuerdoPrestamo: this.operation.acuerdoPrestamo,
            data: {
                drTemplate: 'polizacconsumo',
                applicationName: 'web_consumo',
                variableParams: {
                    firmaEntidad1: firma1,
                    firmaEntidad2: firma2,
                    numeroSucursal: this.operation.cuentaConsumo.substring(4, 8).toString(),
                    sucursal: this.operation.cuentaConsumo.substring(4, 8),
                    cuentaIban: this.formatIBAN(this.loanData.cuentaDomiciliacion),
                    cuentaOperativa: this.operation.acuerdoPrestamo,
                    fecha: this.getFormattedDate(today).dmy,
                    numeroPrestamo: this.operation.acuerdoPrestamo,

                    nombreClienteUno: this.operation.nombrePrimerTitular,
                    codigoDomicilioTipoViaIntervinienteUno: this.operation.domicilioTipoViaPrimerTitular,
                    nombreViaIntervinienteUno: this.operation.domicilioNombreViaPrimerTitular,
                    domicilioNumeroIntervinienteUno: this.operation.domicilioNumeroPrimerTitular,
                    // domicilioPortalIntervinienteUno
                   //  domicilioEscaleraIntervinienteUno
                    domicilioPlantaIntervinienteUno: this.operation.domicilioPlantaPrimerTitular,
                    domicilioPuertaIntervinienteUno: this.operation.domicilioPuertaPrimerTitular,
                    localidadIntervinienteUno: this.operation.domicilioLocalidadPrimerTitular,
                    codigoPostalIntervinienteUno: this.operation.domicilioPostalPrimerTitular,
                    provinciaIntervinienteUno: this.operation.domicilioProvinciaPrimerTitular,
                    paisResidenciaIntervinienteUno: this.operation.paisResidenciaPrimerTitular,
                    idExternoUno: this.operation.idExternoPrimerTitular,

                    existeSegundoComprador: existeSegundoComprador,
                    nombreClienteDos: this.operation.nombreSegundoTitular,
                    codigoDomicilioTipoViaIntervinienteDos : domicilioTipoViaSegundoComprador,
                    nombreViaIntervinienteDos : domicilioNombreViaSegundoComprador,
                    domicilioNumeroIntervinienteDos : domicilioNumeroSegundoComprador,
                    // domicilioPortalIntervinienteDos = port // No lo tengo this.operation/detail. idem
                    // domicilioEscaleraIntervinienteDos = esc // No lo tengo this.operation/detail. idem
                    domicilioPlantaIntervinienteDos : domicilioPlantaSegundoComprador,
                    domicilioPuertaIntervinienteDos : domicilioPuertaSegundoComprador,
                    localidadIntervinienteDos : domicilioLocalidadSegundoComprador,
                    codigoPostalIntervinienteDos : domicilioPostalSegundoComprador,
                    provinciaIntervinienteDos : domicilioProvinciaSegundoComprador,
                    paisResidenciaIntervinienteDos : domicilioPaisSegundoComprador,
                    idExternoDos: this.operation.idExternoSegundoTitular,

                    mailInterviniente : this.operation.mailPrimerTitular,
                    telefonoInterviniente : this.operation.telefonoPrimerTitular,
                    movilInterviniente : this.operation.telefonoMovilPrimerTitular,

                    finalidad: this.operation.finalidad,
                    precioBien: this.formatPrice(parseFloat(this.operation.importeBien)).formatted,
                    capitalConcedido: importe,
                    precioBiencapitalConcedido: importeTotalFinanciado,
                    interesAnualUno: this.formatPrice(parseFloat(this.loanData.interesNominal)).formatted,
                    // Se ha quitado interesAnualDos: this.formatPrice(parseFloat(this.loanData.interesDemora)).formatted,
                    tae: this.formatPriceTAE(parseFloat(tae)).formatted,
                    frecuenciaCapital: this.getPeriodicity(this.loanData.frecuenciaAmortizacion),
                    tipoCuota: this.getCodAmortizacion(this.loanData.codigoAmortizacionCuota),
                    tipoCuotaAmortizacion: this.getCodAmortizacion(this.loanData.codigoAmortizacionCuota),
                    frecuenciaIntereses: this.getPeriodicity(this.loanData.frecuenciaLiquidacion),
                    formaPagoLiquidacion: this.getCodPayform(this.loanData.modalidadLiquidacion),
                    amortizacion:  this.loanData.diaPagoAmortizacion,
                    liquidacionIntereses: this.loanData.diaPagoLiquidacion,
                    carencia: this.formatPrice(parseFloat(this.operation.carencia)).num,
                    fechaVencimiento: fechaVencimiento,
                    comisionApertura: comisionApertura,
                    comisionEstudio: comisionEstudio,
                    comisionAmortizacion: comisionAmortizacion,
                    comisionImpagado: this.formatPrice(parseFloat(this.loanData.comisionReclaReciImp)).formatted,
                    comisionCancelacion: comisionCancelacion,
                    gastosCorreo: this.formatPrice(parseFloat(this.loanData.comisionCorreo)).formatted,
                    idDeudor: this.operation.idExternoPrimerTitular,
                    nombreDeudor: this.operation.nombrePrimerTitular,
                    ibanDeudor: this.operation.cuentaDomiciliacion,
                    fechaFirma: this.getYearMonth(today),
                    clave: this.operation.id,
                    nombreDoc: 'poliza',
                    tipoDoc: '0',
                    plazo: this.operation.duracion, 
                    pagoDeudor: 'Pago recurrente'
                }
            }
        };
          // Post request to "userInProcess" to store first NOT client data in DB
          this.loadStore('LOAD_CENTERADOCUMENT_LIST', data);
        
      },
      generateINE(operationsnoneconomic) {
        const today = new Date();
        let fechaFin = '';
        let importeLetras = '';
        let importe = '';
        let interesesTotales = '0';
        let tae = '0';
        let comisionApertura = '';
        let comisionEstudio = '';
        let entidad = this.agreementList[0].CTA_ASO.substring(0,4);
        let firma1 = entidad;
        let firma2 = entidad;
        if (operationsnoneconomic.importeMod) {
            importeLetras = parseFloat(operationsnoneconomic.importeMod);
            importe = this.formatPrice(parseFloat(operationsnoneconomic.importeMod)).formatted;
            comisionApertura = this.formatPrice(parseFloat(operationsnoneconomic.importeMod)*parseFloat(this.loanData.comisionApertura)/100).formatted;
            comisionEstudio = this.formatPrice(parseFloat(operationsnoneconomic.importeMod)*parseFloat(this.loanData.comisionEstudio)/100).formatted;
        } else {
            importeLetras = parseFloat(operationsnoneconomic.importe);
            importe =this.formatPrice(parseFloat(operationsnoneconomic.importe)).formatted;
            comisionApertura = this.formatPrice(parseFloat(operationsnoneconomic.importe)*parseFloat(this.loanData.comisionApertura)/100).formatted;
            comisionEstudio = this.formatPrice(parseFloat(operationsnoneconomic.importe)*parseFloat(this.loanData.comisionEstudio)/100).formatted;
        }
        if (this.loanData.interesTotales) {
            interesesTotales = this.loanData.interesTotales;
        }
        if (parseFloat(operationsnoneconomic.tipoOperacion) == 113 || parseFloat(operationsnoneconomic.tipoOperacion) == 133 ) {
            tae = operationsnoneconomic.tae;
        } else {
            if (this.loanData.tae) {
                tae = this.loanData.tae
            } else if (operationsnoneconomic.tae) {
                tae = operationsnoneconomic.tae;
            }
        }        
        if (this.loanData.fechaFinPrestamo) {
            fechaFin = this.getFormattedDate(this.loanData.fechaFinPrestamo.substring(0,10)).dmy;
        };
        let domicilioTipoViaSegundoComprador = ''; 
        let domicilioNombreViaSegundoComprador = ''; 
        let domicilioNumeroSegundoComprador = ''; 
        let domicilioProvinciaSegundoComprador = ''; 
        let domicilioLocalidadSegundoComprador = ''; 
        let domicilioPostalSegundoComprador = ''; 
        let existeSegundoComprador = 'noVisible';
        if (operationsnoneconomic.domicilioTipoViaSegundoTitular){
          domicilioTipoViaSegundoComprador = operationsnoneconomic.domicilioTipoViaSegundoTitular; 
          domicilioNombreViaSegundoComprador = operationsnoneconomic.domicilioNombreViaSegundoTitular;
          domicilioNumeroSegundoComprador = operationsnoneconomic.domicilioNumeroSegundoTitular; 
          domicilioProvinciaSegundoComprador = operationsnoneconomic.domicilioProvinciaSegundoTitular; 
          domicilioLocalidadSegundoComprador = operationsnoneconomic.domicilioLocalidadSegundoTitular; 
          domicilioPostalSegundoComprador = operationsnoneconomic.domicilioPostalSegundoTitular; 
          existeSegundoComprador = '';
        }
        if (operationsnoneconomic.domicilioTipoViaAvalista){
          domicilioTipoViaSegundoComprador = operationsnoneconomic.domicilioTipoViaAvalista; 
          domicilioNombreViaSegundoComprador = operationsnoneconomic.domicilioNombreViaAvalista; 
          domicilioNumeroSegundoComprador = operationsnoneconomic.domicilioNumeroAvalista; 
          domicilioProvinciaSegundoComprador = operationsnoneconomic.domicilioProvinciaAvalista; 
          domicilioLocalidadSegundoComprador = operationsnoneconomic.domicilioLocalidadAvalista; 
          domicilioPostalSegundoComprador = operationsnoneconomic.domicilioPostalAvalista; 
          existeSegundoComprador = '';
        }
        let data = {
            codigoEntidad: this.user.codigoEntidad,
            cuenta: operationsnoneconomic.cuentaConsumo,
            acuerdoPrestamo: operationsnoneconomic.acuerdoPrestamo,
            data: {
            drTemplate: 'inecconsumo',
            applicationName: 'web_consumo',
            variableParams: {
                firmaEntidad1 : firma1,
                firmaEntidad2 : firma2,
                nombrePrimerComprador: operationsnoneconomic.nombrePrimerTitular,
                idExternoPrimerComprador: operationsnoneconomic.idExternoPrimerTitular,
                nombreSegundoComprador: operationsnoneconomic.nombreSegundoTitular,
                idExternoSegundoComprador: operationsnoneconomic.idExternoSegundoTitular,
                nombrePrestamo: "Préstamo Automático Consumo",
                // tipoCredito: "Tipo credito a fuego", DICE María que no va el campo aquí 
                importePrestamo: importe,
                importePrestamoLetras: this.NumeroALetras(importeLetras),
                condicionesFondos: "El consumidor encomienda a la Entidad el pago al establecimiento vendedor de la adquisición realizada o del servicio prestado, obligándose al pago de su importe y de sus intereses; en la forma prevista en el presente documento.",
                duracionContrato: operationsnoneconomic.duracion,
                fechaVencimiento: fechaFin,
                tipoCuota: this.getCodAmortizacion(this.loanData.codigoAmortizacionCuota),
                frecuenciaCapital: this.getPeriodicity(this.loanData.frecuenciaAmortizacion),
                frecuenciaIntereses: this.getPeriodicity(this.loanData.frecuenciaLiquidacion),
                formaPagoLiquidacion: this.getCodPayform(this.loanData.modalidadLiquidacion),
                plazoAmortizacion: operationsnoneconomic.duracion.toString(),
                liquidacionIntereses: this.getPeriodicity(this.loanData.frecuenciaLiquidacion),
                amortizacion: this.getCodAmortizacion(this.loanData.codigoAmortizacionCuota),
                precioBien: this.formatPrice(parseFloat(operationsnoneconomic.importeBien)).formatted,
                capitalConcedido: importe,
                interesesTotales: this.formatPrice(parseFloat(interesesTotales)).formatted,
                comisionApertura: comisionApertura,
                comisionEstudio: comisionEstudio,
                gastosCorreo: this.formatPrice(parseFloat(this.loanData.comisionCorreo)).formatted,
                importeTotal: this.formatPrice(parseFloat(this.loanData.importeTotal)).formatted,
                garantia: "Personal",
                interesDeudorAnual: this.formatPrice(parseFloat(this.loanData.interesNominal)).formatted,
                tae: this.formatPriceTAE(parseFloat(tae)).formatted,
                comisionImpagado: this.formatPrice(parseFloat(this.loanData.comisionReclaReciImp)).formatted,
                fechaFirma: this.getYearMonth(today),
                nombrePrimerPrestatario: operationsnoneconomic.nombrePrimerTitular,
                nombreSegundoPrestatario: operationsnoneconomic.nombreSegundoTitular,
                clave: operationsnoneconomic.id,
                sucursalEntidad: operationsnoneconomic.codigoOficinaPrestamo,
                nombreDoc: 'ine',
                tipoDoc: '1',
                fechaFin: fechaFin,
                domicilioTipoViaPrimerComprador : operationsnoneconomic.domicilioTipoViaPrimerTitular, 
                domicilioNombreViaPrimerComprador : operationsnoneconomic.domicilioNombreViaPrimerTitular, 
                domicilioNumeroPrimerComprador : operationsnoneconomic.domicilioNumeroPrimerTitular, 
                domicilioProvinciaPrimerComprador : operationsnoneconomic.domicilioProvinciaPrimerTitular, 
                domicilioLocalidadPrimerComprador : operationsnoneconomic.domicilioLocalidadPrimerTitular, 
                domicilioPostalPrimerComprador : operationsnoneconomic.domicilioPostalPrimerTitular, 
                existeSegundoComprador: existeSegundoComprador,
                domicilioTipoViaSegundoComprador : domicilioTipoViaSegundoComprador, 
                domicilioNombreViaSegundoComprador : domicilioNombreViaSegundoComprador, 
                domicilioNumeroSegundoComprador : domicilioNumeroSegundoComprador, 
                domicilioProvinciaSegundoComprador : domicilioProvinciaSegundoComprador, 
                domicilioLocalidadSegundoComprador : domicilioLocalidadSegundoComprador, 
                domicilioPostalSegundoComprador : domicilioPostalSegundoComprador, 
            }
        }
        };    
        this.$root.$loading.start();
        this.loadStore('LOAD_CENTERADOCUMENT_LIST', data);
      },
      consultINE(clipID) {
        this.loadStore('LOAD_CENTERARECUPERACION_LIST', clipID);
        
      },
      consultPolicy(clipID) {
        this.loadStore('LOAD_CENTERARECUPERACION_LIST', clipID);

      },
      operationDetails(props) {
        if (((parseFloat(props.item.situacion) == 2) || (parseFloat(props.item.situacion) == 33) ) && (parseFloat(props.item.estado) == 7 ))  {
            // No hace nada porque es una operación cancelada
        } else {
            if (parseFloat(props.item.situacion) !== 6) {
                this.acuerdoPrestamo = props.item.acuerdoPrestamo;
                let busqueda = {};
                if (props.item.idInternoPePrimerTitular) {
                    busqueda['idInternoPePrimerTitular'] = props.item.idInternoPePrimerTitular;
                }
                if (props.item.idInternoPeSegundoTitular) {
                    busqueda['idInternoPeSegundoTitular'] = props.item.idInternoPeSegundoTitular;
                }
                if (props.item.idInternoPeAvalistaTitular) {
                    busqueda['idInternoPeAvalistaTitular'] = props.item.idInternoPeAvalistaTitular;
                }
                busqueda['id'] = props.item.id;
                busqueda['codigoEntidad'] = this.user.codigoEntidad;
                this.$root.$loading.start();
                this.auxProps = props;
                this.loadStore('LOAD_OPERATIONNOECONOMIC_DETAILS',  busqueda);
                
            } else {
                props.expanded = !props.expanded;
            }
        }
      },
    }
  }
</script>

<style lang="scss">
    .material-icons{
        font-size: 15px;
        margin-left: 5px;
    }
    .datepicker-text{
        width: 80%;
        float: left;
    }
    .datepicker-icon{
        width: 10%;
        float: left;
        font-size: 20px;
        margin-top: 5px;
    }
</style>
