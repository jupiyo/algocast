// --- Directions
// Given an integer, return an integer that is the reverse
// ordering of numbers.
// --- Examples
//   reverseInt(15) === 51
//   reverseInt(981) === 189
//   reverseInt(500) === 5
//   reverseInt(-15) === -51
//   reverseInt(-90) === -9

function reverseInt(n) {
    const reversed = n
    .toString()
    .split('')
    .reverse()
    .join('');

    return parseInt(reversed) * Math.sign(n)
}

// COMENTAR (Y DESCOMENTAR ABAJO) PARA PROBAR EN NAVEGADOR
console.log(reverseInt(23));
console.log(reverseInt(-32));

// DESCOMENTAR PARA PROBAR EJECUTAR TESTS UNITARIOS
module.exports = reverseInt;
