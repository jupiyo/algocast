// --- Directions
// Given a string, return a new string with the reversed
// order of characters
// --- Examples
//   reverse('apple') === 'leppa'
//   reverse('hello') === 'olleh'
//   reverse('Greetings!') === '!sgniteerG'

// SOLUCIÓN 1

/* function reverse(str) {
    return str.split('').reverse().join('');
} */

// ----------------------------------

// SOLUCIÓN 2

function reverse(str) {
    return str.split('').reduce((act, sum)=> sum + act, '');
}

// ----------------------------------

// SOLUCIÓN 3

/* function reverse(str) {
    
} */



// ---COMENTAR/DESCOMENTAR PARA NAVEGADOR--

reverse('  mi prima es rara');


// ---COMENTAR/DESCOMENTAR PARA TEST-----

// module.exports = reverse;
