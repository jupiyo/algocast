# Algoritmos y de estructuras de datos en JS con pruebas unitarias (Jest)

1) No es necesario instalar Jest (ni ninguna otra dependencia) en el proyecto

1. Basta con instalarlo a nivel global **((sudo) npm i -g jest)**.

2) Antes de ejecutar los tests unitarios:
1. Descomentar el module.exports
2. Comentar la llamada a la función en el archivo a probar (Ej: reverseString())
3. Ejecutar test: Ej: **jest reversestring/test.js**
    
3) Debuguear en navegador:
1. Linkar el .js a debuguear en el index.html del raíz
2. Descomentar la llamada a la función en el archivo a probar (Ej: reverseString())
3. Comentar el module.exports

4) Orden de los ejercicios:

    String Reversal

    Palindromes

    Integer Reversal

    MaxChars

    The Classic FizzBuzz!

    Array Chunking

    Anagrams

    Sentence Capitalization

    Printing Steps

    Two Sided Steps - Pyramids

    Find The Vowels

    Enter the Matrix Spiral

    Runtime Complexity

    Runtime Complexity in Practice - Fibonacci

    The Queue

    Underwater Queue Weaving

    Stack 'Em Up With Stacks

    Two Become One

    Linked Lists

    Find the Midpoint

    Circular Lists?

    Step Back From the Tail

    Building a Tree

    Tree Width with Level Width

    My Best Friend, Binary Search Trees

    Validating a Binary Search Tree

    Back to Javascript - Events

    Building Twitter - A Design Question

    Sorting With BubbleSort

    Sort By Selection
